//
//  ViewController.swift
//  RedBlue
//
//  Created by Ryan Grewatz on 10/4/16.
//  Copyright © 2016 Ryan Grewatz. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var redPic: UIImageView!
    @IBOutlet weak var bluePic: UIImageView!
    @IBOutlet var pics: [UIImageView]!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func toggleHideBlueAction(_ sender: AnyObject) {
        bluePic.isHidden = !bluePic.isHidden
    }

    @IBAction func toggleHideRedAction(_ sender: AnyObject) {
        redPic.isHidden = !redPic.isHidden
    }
    
    @IBAction func toggleHideBothAction(_ sender: AnyObject) {
        pics.forEach{ $0.isHidden = !$0.isHidden}
    }
}

